setEvents = ->
	$('.change-enable-status').on 'click', ->
		$(".loading").show()
		check = (el = @).parentNode.previousElementSibling.firstElementChild
		value = !check.checked
		$.get '/pcs/orders/change_cache_status', {
				process_type: currentProcessType.value, version: $(@).data('v'), value
			}, ->
				$(".loading").hide()
				el.innerHTML = value and 'Switch Off' or 'Switch On'
				check.checked = value
setEvents()

processTypePicker.onchange = ->
	$(".loading").show()
	xhr = new XMLHttpRequest
	xhr.open 'GET', "/pcs/orders/process_cache?process_type=#{@value}", false
	xhr.onload = ->
		if @status is 200
			processOutput.innerHTML = @response
			setEvents()
		else
			processOutput.innerHTML = ''
			Application.messenger.show [['error', @response]]
		$(".loading").hide()
	xhr.send()