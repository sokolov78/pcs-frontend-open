class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception, except: [:demo_sign_in]
  class_attribute :hbw_available
  self.hbw_available = false
  helper_method :hbw_available?
  require 'rest-client'
  before_action -> { ENV['DISABLE_PRY'] = nil }

  before_filter :miniprofiler, if: "Rails.env.development?"
  # before_filter :get_user_list

  protected

  def get_user_signed_in
    @current_user = current_user
  end

  def miniprofiler
    Rack::MiniProfiler.authorize_request
  end
  # @rest_api = RestApi.new

  def json_request?
    request.format.json?
  end

  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(_resource_or_scope)
    new_user_session_path
  end

  def flash_messages
    %i(success notice error).reduce([]) do |messages, type|
      if flash.key?(type)
        messages + [[type, flash[type]]]
      else
        messages
      end
    end
  end

  def authenticate_user!
    return true if session[:user_email]
    redirect_to controller: "sessions", action: "new"
  end

  def current_user
    if @current_user.nil?
      $current_user = get_current_user
    else
      @current_user
    end
  end

  def get_current_user
    get_user_from_api(session[:user_email], session[:password])
  end

  def admin?(user)
    user['role'].downcase == 'admin'
  end

  helper_method :flash_messages
  helper_method :authenticate_user!
  helper_method :current_user
  helper_method :admin?
  helper_method :getUserList

  def get_user_from_api(email, password)
    begin
      Apiv2.sign_in(email, password)
    rescue => e
      return false
    end
  end

  def getUserList
    @get_user_list.nil? ? find_user_list : @get_user_list
  end

  def get_user_list
    @get_user_list = getUserList
  end

  def find_user_list
    JSON.parse(RestClient.get BACKEND_API_CONFIG['url'] + "user/list")
  end

end
