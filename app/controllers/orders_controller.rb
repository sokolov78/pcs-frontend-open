class OrdersController < API::BaseController
  before_action :authenticate_user!
  self.hbw_available = true

  respond_to :html
  before_filter :get_user_signed_in
  before_action :resource_set, only: [:create, :show, :new]

  def change_cache_status
    Apiv2.switch_process_version params[:process_type], params[:version], params[:value]
    render nothing: true
  end

  def process_cache
    @current = params[:process_type]
    if @current
      res = Apiv2.get_process_versions @current
      if res.code == '200'
        @versions = JSON.parse res.body
        render 'orders/_user_cache', layout: false
      else
        render text: res.body, status: res.code
      end
    else
      @list = Apiv2.get_process_list
      @current = @list.first
      res = Apiv2.get_process_versions @current
      if res.code == '200'
        @versions = JSON.parse res.body
      else
        @versions = []
        flash.now[:error] = res.body
      end
    end
  end

  def show
    if params['unclaim']
      res = Apiv2.unclaim_task params[:id]
      if res.code == '200'
        flash = {success: res.body}
      else
        flash = {error: res.error}
      end
      redirect_to '/pcs/orders', flash: flash
    else
      res = Apiv2.claim_task params[:id]
      if res.code == '200'
        @order[:raw] =  JSON.parse(res.body)
        @order[:data] = prepared_order_fields(@order[:raw]['fields'])
        @order[:code] = @order[:raw]['code']
      elsif res.code == '410'
        flash = {success: res.body}
        redirect_to '/pcs/orders', flash: flash
      else
        flash = {error: res.body}
        redirect_to '/pcs/orders', flash: flash
      end
    end
  end

  # GET /orders/search_by/{:code | :ext_code}/
  def search_by
    @criteria = "#{Order.human_attribute_name(params[:field])}: "\
                "'#{params[:value]}'"
    resource_set Order.find_by!(params[:field] => params[:value])
    render action: :show
  end

  def index
    #Main staff, fetch orders
    #@url = BACKEND_API_CONFIG['url'] + getQueryUrl
    #JSON.parse(RestClient.get(BACKEND_API_CONFIG['url'] + getQueryUrl))
    @orders = get_orders_from_api
    @orders = @orders#.paginate(:page => params[:page], :per_page => 10)
    #Filters staff
    @datetime_from = params[:created_at_from]
    @datetime_to = params[:created_at_to]

    @params_s = request.query_parameters
    if !params['user_id']&&!(@params_s[:filter]||@params_s[:commit])
      @users_for_pick = [current_user['email']]
    else
      @users_for_pick = [];
      @users_for_pick = params['user_id'] if(params['user_id'])
    end
  end

  def edit
  end

  def new
    if request.xml_http_request? #After order type chosen render fields else just render 'new'view
      form_id = params[:order_type_id]

      @order[:raw] = Apiv2.get_process_form form_id
      @order[:data] = prepared_order_fields(@order[:raw]['fields'])
      @order[:code] = form_id

      render partial: params[:partial_name]
    end
  end

  def create
    order_form = params[:order_form]
    form_id = params[:order_type_id]
    if params[:id]
      json = JSON.parse(Apiv2.claim_task(params[:id]).body)
      json['taskId'] = params[:id]
    else
      json = Apiv2.get_process_form(form_id)
    end
    unless json['fields'].nil?
      json['fields'].each do |field|
        value = order_form[field['id']]
        if field['type'] == 'bool'
          value = value == '0' ? false : true
        end
        field['value'] = value
      end
    end
    json['attribute'] = {timestamp: DateTime.now.to_time.to_i}
    res = Apiv2.bpeDo json

    respond_to do |format|
      format.html do
       if res.code == '200'
        flash = {success: res.body}
       else
        flash = {error: res.body}
       end
       # binding.pry
       redirect_to '/pcs/orders', flash: flash
      end
      format.js do
        if res.code == '200'
          render json: res.body.to_json, status: 200
        else
          render json: @error.to_json, status: 402
        end
        # binding.pry
      end
    end

    # fields = {}
    # #If create action performed after new action
    # if params[:id].blank?
    #   url = BACKEND_API_CONFIG['url'] + 'forms/json/' +  params[:order_type_id]
    # else # If action create being performed after show
    #   url = BACKEND_API_CONFIG['url'] + 'bpe/claimTask/'
    #   url += '?userID=' + current_user['email']
    #   url += '&taskID=' + params[:id]
    # end
    # @order[:raw] = JSON.parse(RestClient.get(url))
    # raw = @order[:raw]

    # raw['attribute'] = {
    #   'userId' => current_user['email'],
    #   'timestamp' => DateTime.now.to_time.to_i,
    #   "apiKey" => current_user['api_token']
    # }

    # unless raw['fields'].nil?
    #   raw['fields'].each do |field|
    #     if field['type'] == 'bool'
    #       val = params[:order_form][field['id']]
    #       params[:order_form][field['id']] = val == '0' ? false : true
    #     end
    #     field['value'] = params[:order_form][field['id']]
    #   end
    # end
    # begin
    #   @res = RestClient.post BACKEND_API_CONFIG['url'] + "bpe/do", raw.to_json, :content_type => :json, :accept => :json
    # rescue => e
    #   @error = e.response.to_str
    # end
    # success = !@res.nil? && @res.code == 200

    # respond_to do |format|
    #   format.html do
    #    if success
    #     flash = {success: 'Success!'}
    #    else
    #     flash = {error: res.body}
    #    end
    #    redirect_to '/orders', flash: flash
    #   end
    #   format.js do
    #     if success
    #       render json: @res.body.to_json, status: 200
    #     else
    #       render json: @error.to_json, status: 402
    #     end
    #   end
    # end





    # raw = []
    # raw_json_fields =  JSON.parse(@res.body)['fields']#.to_json
    # raw_json_fields.each do |order_type|

    # raw.push({
    #   :type => order_type['type'],
    #   :name => order_type['id'],
    #   :value => order_type['value'] || '',
    #   :label => order_type['label'],
    #   :can_edit => order_type['canEdit'],
    #   :order => order_type['order'],
    #   :value_set => order_type['valuesSet'] || ''
    #   })

    # end

    # raw.sort_by! {|order|
    #   order[:order]
    # }
    # @order = {}
    # @order[:data] = raw
    # @order[:code] = JSON.parse(@res.body)['code']
    # @order[:status] = @res.code.to_s + " " + @res.headers.to_s
  end
  def prepared_order_fields (raws)
    return if raws.nil?

    order_types = [];
    raws.each do |order_type|
      order_types.push({
        :type => order_type['visible'] ? order_type['type'] : 'hidden',
        :name => order_type['id'],
        :value => order_type['value'] || '',
        :label => order_type['label'],
        :can_edit => order_type['canEdit'],
        :order => order_type['order'],
        :value_set => order_type['valuesSet'] || ''
        })
    end
    order_types.sort_by! {|order|
      order[:order]
    }
    order_types
  end

  def get_orders_from_api
    Apiv2.get_page({offset: 0, size: 0, tasks: [], processes: []})
  end

  def getDateFromParams(key)
    date_raw = URI.unescape(params[key])[0...10]
    time_raw = URI.unescape(params[key])[11...19]

    date = Date.strptime(date_raw, "%m/%d/%Y")
    time = Time.strptime(time_raw, "%H:%M %p")
    return date.strftime("%Y-%m-%d") + "T" + time.strftime("%H:%M:%S") + ".434%2B0400"
  end

  def getParamsForApi
    parameters = {}
    params.each do |p|
      case p[0]
      when "order_type_id"
        parameters['orderType'] = params['order_type_id'] if params['order_type_id'] != ""
      when "state"
        parameters['orderStatus'] = params['state'] if params['state']  != ""
      when "archived"
        parameters['archived'] = params['archived'] if params['archived'] != ""
      when "user_id"
        next if params['user_id'] == ""
        str = ""
        params['user_id'].each_with_index do |u, index|
          str += "&userID=" if index != 0
          str += u
        end
        parameters['userID'] = str
      when "created_at_from"
        parameters['dateFrom'] = getDateFromParams('created_at_from') if params['created_at_from'] != ""
      when "created_at_to"
        parameters['dateTo'] = getDateFromParams('created_at_to') if params['created_at_to'] != ""
      when "value"
        parameters['code'] = params['value'] if params['value'] != ""
      when "code"
        parameters['businessID'] = params['code'] if params['code'] != ""
      else
      end
    end


    params_str = ""
    parameters.each { |p|
      params_str += p[0] + "=" + p[1] + "&"
    }
    params_str

  end

  def getQueryUrl

    params_str = getParamsForApi
    return "query?" + params_str unless params_str.empty?

    user = "&userID=#{current_user['email']}" if params['commit'] != "Search" && current_user
    user ||= ''

    "query?archived=0#{user}"
  end
  #**************USELESS SHIT*****************
  def update
    @order.data = params[:order]

    if @order.save
      redirect_to order_url(@order.code)
    else
      render 'edit'
    end
  end

  def set_order
    @order = Order.new
  end
  def get_field_def( name )

  end
  protected

  def resource_get
    instance_variable_get("@#{resource_name}")
  end

  helper_method \
  def list_filter
    @filter ||= ListOrdersFilter.new(current_user, filter_params)
  end

  def filter_params
    # user_ids = params[:filter] ? params[:user_id] : [current_user.id, 'empty']
    user_ids = {}
    params.permit(:state, :order_type_id, :archived,
                  :created_at_from, :created_at_to,
                  :filter, user_id: []).reverse_merge(user_id: user_ids,
                                                      archived: '0',
                                                      state: nil)
  end

  def record_not_found
  end

  def resource_set(resource = nil)
    @order = {}
  end
end
