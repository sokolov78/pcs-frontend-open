require 'digest'

class SessionsController < ApplicationController

  def new
  end

  def create
    user = get_user_from_api(
      params[:user]['email'],
      'paswhash:' + Digest::MD5.base64digest(params[:user]['password'])
    )
    if user then
      session[:user_email] = user['emailID']
      session[:password] = user['pwdHash']
      redirect_to :list_orders, :notice => "Logged in!"
    else 
      flash.now.alert = "Invalid email or password"
      render "new"
    end
  end

  def destroy
    session[:user_email] = nil
    session[:password] = nil
    redirect_to :log_in, :notice => "Logged out!"
  end

  def demo_sign_in
    session[:user_email] = params[:emailID]
    session[:password] = params[:pwdHash]
    redirect_to :list_orders, notice: "Logged in!"
  end
end