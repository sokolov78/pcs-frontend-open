module ApplicationHelper

  def my_items
    if action_name == "index" && controller_name == "orders"
      @orders['myItems']
    else
      Apiv2.get_my_tasks
    end
  end

  def roles_for_select
    JSON.parse(RestClient.get(BACKEND_API_CONFIG['url'] + 'types/roles'))
    #User.roles.keys.map { |role| [t(User.role_i18n_key(role)), role] }
  end

  def prev_page_number (cur_page)
    pre_page = cur_page.to_i - 1
    (pre_page < 0 ? 0 : pre_page).to_s
  end

  def next_page_number (cur_page)
    (cur_page.to_i + 1).to_s
  end

  def order_states_for_select

    # @url = BACKEND_API_CONFIG['url'] + 'bpe/retrieveWorkListPage'
    # data = {
    #     "userID" => current_user['email'],
    #     "pageOffset" => (params[:page].to_i < 0 ? 0 : params[:page].to_i),#per page
    #     "pageSize" => 100,
    #     "task" => [ ],
    #     "process" => [ ],
    #     "processNamesAsProzessname" => [ ]
    # }
    # raw = JSON.parse(RestClient.get("#{@url}?#{data.to_query}", :content_type => :json, :accept => :json))['tasksNames']

    @orders['tasksNames'].map do |order_type|
      [order_type, order_type]
    end

  end

  def order_types_for_select
    if params['action'] == 'index'
      # @url = BACKEND_API_CONFIG['url'] + 'bpe/retrieveWorkListPage'
      # data = {
      #     "userID" => current_user['email'],
      #     "pageOffset" => (params[:page].to_i < 0 ? 0 : params[:page].to_i),#per page
      #     "pageSize" => 100,
      #     "task" => [ ],
      #     "process" => [ ],
      #     "processNamesAsProzessname" => [ ]
      # }
      # raw = JSON.parse(RestClient.get("#{@url}?#{data.to_query}", :content_type => :json, :accept => :json))['processNames']

      @orders['processNames'].map do |state|
        [state, state]
      end
    else
      Apiv2.new_processes_list.map do |state|
        [state[1], state[0]]
      end
    end

  end

  def boolean_indicator(boolean)
    boolean ? tag(:span, class: %w(glyphicon glyphicon-ok)) : ''
  end

  def order_state_indicator(order)
    title = I18n.t(Order.state_i18n_key(order.state))
    icon = if order.to_execute?
             'fa-square-o'
           elsif order.in_progress?
             'fa-gears'
           else
             'fa-check-square-o'
           end

    "<span><i class='fa #{icon}'></i> #{title}</span>".html_safe
  end

  def yes_no
    %w(yes no).each_with_index.map do |e, i|
      [t("helpers.yes_no.#{e}"), i.zero?]
    end
  end

  def loc_date(date)
    case
      when Time, DateTime then I18n.l(date, format: :date)
      when String then I18n.l(DateTime.iso8601(date), format: :date)
      else ''
    end
  end

  def loc_datetime(datetime)
    case datetime
      when Time, DateTime then I18n.l(datetime, format: :datetime)
      when String then I18n.l(DateTime.iso8601(datetime), format: :datetime)
      when Fixnum then DateTime.strptime((datetime.to_f / 1000).to_s, '%s').to_s.gsub('T', ' ').gsub(/\+.*/, '')
      else ''
    end
  end

  def bootstrap_class_for(flash_type)
    case flash_type
    when 'success' then 'alert alert-dismissable alert-success'   # Green
    when 'error'   then 'alert alert-dismissable alert-danger'    # Red
    when 'alert'   then 'alert alert-dismissable alert-warning'   # Yellow
    when 'notice'  then 'alert alert-dismissable alert-info'      # Blue
    else flash_type.to_s
    end
  end

  def hbw_options
    default_options = {
      widgetPath: '/widget',
      tasksMenuContainer: '#hbw-tasks',
      tasksMenuButtonContainer: '#hbw-tasks-list-button',
      locale: I18n.locale
    }

    if defined? @hbw_options
      default_options.merge(@hbw_options)
    else
      default_options
    end
  end

  def get_filter_params(params)
    str = ''
    unless params.blank?
      params.each do |key, value|
        str += "&#{key}=#{value}" if ['state', 'order_type_id'].include?(key)
      end
    end
    str
  end

  if Rails.env != 'production'
    def stylesheet_link_tag(name, options = {})
      res = ''
      res += "<link rel='stylesheet'"
      for k, v in options
        res += " #{k}='#{v}'"
      end
      res += " href='/assets/#{name}.css'>"
      raw res
    end

    def javascript_include_tag(*names)
      res = ''
      names.each do |name|
        res += "<script type='text/javascript' src='/assets/#{name}.js'></script>"
      end
      raw res
    end
  end
end
