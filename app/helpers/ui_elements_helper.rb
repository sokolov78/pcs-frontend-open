# rubocop:disable Metrics/MethodLength
module UiElementsHelper
  include DatetimeFormat

  def datetime_picker(name, options = {})
    if options[:value].present?
      value = options[:value]
    else
      value = nil
    end

    content_tag(:div, class: 'form-group') do
      content_tag(:div,
                  data: { showClear: true },
                  class: "input-group date datetime-picker #{options[:class]}") do
        content_tag(:input,
                    name: name,
                    type: 'text',
                    class: 'form-control',
                    value: value) do
          content_tag(:span, class: 'input-group-addon') do
            content_tag(:span, class: 'glyphicon glyphicon-calendar') {}
          end
        end
      end
    end
  end

  def user_picker(name, value: [])
    content_tag(:select, name: name.to_s + '[]',
                         data: {
                           allowClear: true,
                           placeholder: I18n.t('helpers.enter_user'),
                           ajax: { url: lookup_users_path },
                         },
                         multiple: true,
                         class: 'user-picker') do
      #value.map { |u| [u[0], u[0]] }

      options_for_select(value, value)
    end
  end

  def order_state_picker(name, value: nil)
    content_tag(:select, name: name,
                         data: {
                           allowClear: true,
                           placeholder: I18n.t('helpers.enter_state')
                         },
                         class: 'form-control order_state-picker') do
                           '<option></option>'.html_safe + options_for_select(order_states_for_select, value)
                         end
  end

  def order_type_picker(name, value: nil)
    content_tag(:select, name: name,
                         data: {
                           allowClear: true,
                           placeholder: I18n.t('helpers.enter_order_type')
                         },
                         value: value,
                         class: 'form-control order_type-picker right-element') do
                          '<option></option>'.html_safe + options_for_select(order_types_for_select, value)
                         end
  end

  def dropdown_field_picker(name, values, input_html: {})
    content_tag(:select, name: name,
                         data: {
                           allowClear: true,
                           placeholder: 'Select option'
                         },
                         input_html[:disabled].to_s + '' => '',
                         value: input_html[:selected],
                         class: 'form-control order_type-picker') do
                          '<option></option>'.html_safe + options_for_select(values, input_html[:selected])
                         end
  end

  def order_archived_picker(name, value: nil)
    content_tag(:select, name: name,
                         data: {
                           allowClear: true,
                           placeholder: I18n.t('helpers.enter_archived')
                         },
                         class: 'form-control order_archived-picker') do
        '<option></option>'.html_safe + options_for_select([
          [t('helpers.yes_no.yes'), '1'],
          [t('helpers.yes_no.no'), '0']
        ], value)
      end
  end
end
# rubocop:enable Metrics/MethodLength
