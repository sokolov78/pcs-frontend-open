require 'digest'
require 'net/http'
require 'json'

def generate_token secret
	Digest::MD5.base64digest secret + (Time.now.to_i / 60).to_s
end

def post action, user_id, secret, data
	uri = URI(BACKEND_API_CONFIG['url'] + action)
	req = Net::HTTP::Post.new(uri, {
		# Authorization: "#{user_id} #{generate_token secret}",
		Authorization: "#{user_id} a",
		'Content-Type' => 'application/json'
	})
	req.body = data.to_json
	Net::HTTP.start(uri.hostname, uri.port) do |http|
		http.request(req)
	end
end

def get action
	uri = URI(BACKEND_API_CONFIG['url'] + action)
	req = Net::HTTP::Get.new(uri, {
		Authorization: "#{$current_user['emailID']} a",
		'Content-Type' => 'application/json'
	})
	Net::HTTP.start(uri.hostname, uri.port) do |http|
		http.request(req)
	end
end

class Apiv2

	def self.sign_in login, password
		res = post 'user/get', 'login@amigro.net', 'xDcZX8cxCnIPo09llmw', {emailID: login, pwdHash: password}
		if res.code == '200'
			JSON.parse res.body
		else
			false
		end
	end

	def self.get_page opts
		emailID = $current_user['emailID']
		res = post 'dashboard/getPage', emailID, $current_user['apiKey'], {
			userId: emailID,
			pageOffset: (opts[:offset] or 0),
			pageSize: (opts[:size] or 0),
			taskNames: (opts[:tasks] or []),
			processNames: (opts[:processes] or [])
		}
		JSON.parse res.body
	end

	def self.new_processes_list
		res = get "forms/startList?userID=#{$current_user['emailID']}"
		JSON.parse res.body
	end

	def self.get_process_form form_id
		res = get "forms/get?userID=#{$current_user['emailID']}&formID=#{form_id}"
		JSON.parse res.body
	end

	def self.bpeDo json
		post 'bpe/do', $current_user['emailID'], $current_user['apiKey'], json
	end

	def self.claim_task id
		get "bpe/claimTask?taskID=#{id}"
	end

	def self.unclaim_task id
		get "bpe/unclaimTask?taskID=#{id}"
	end

	def self.get_my_tasks
		JSON.parse(get("dashboard/myTasks").body)['myItems']
	end

	def self.get_process_list
		JSON.parse get("forms/getProcessList").body
	end

	def self.get_process_versions name
		get "forms/getProcessVersions?procId=#{name}"
	end

	def self.switch_process_version process_type, version, value
		get "forms/switchProcessVersion?procId=#{process_type}&version=#{version}&value=#{value}"
	end

end