require 'api_constraints'

Rails.application.routes.draw do

  scope 'pcs' do

    delete "/" => "sessions#destroy", :as => "log_out"
    get "log_in" => "sessions#new", :as => "log_in"
    post 'demo-sign-in', to: 'sessions#demo_sign_in'
    get "sign_up" => "users#new", :as => "sign_up"

    get '/sign_in_by_token/:token' => 'sessions#sign_in_by_token'

    root to: 'orders#index', as: :list_orders
    post 'users/log_in' => 'users#log_in'
    resources :sessions
    resources :users do
      post :add, on: :collection
      get :lookup, on: :collection
      put :generate_api_token, on: :member
      delete :clear_api_token, on: :member
    end

    get '/orders/search_by/:field',
        to: 'orders#search_by',
        as: :search_order_by,
        constraints: { field: /code|ext_code/ }

    resources :orders, only: [:show, :edit, :update, :index, :new, :create] do
      collection do
        get :process_cache
        get 'change_cache_status', to: 'orders#change_cache_status'
      end
    end
    post 'orders/:id' => 'orders#create'
    namespace :admin do
      post 'order_types/:order_type_id/update' => 'order_types#update', as: 'update_order_type'
      resources :order_types, only: [:index, :show, :create, :destroy] do
        get :lookup, on: :collection
        put :activate, on: :member
        delete :dismiss,  on: :member
      end
    end

    namespace :api, defaults: { format: :json } do
      scope module: :v1,
            constraints: ApiConstraints.new(version: 1, default: true) do
        actions = [:index, :show, :create, :update, :destroy]

        resources :users, only: actions, constraints: { id: /.*/ }
        resources :orders, only: actions
      end
    end

    mount HBW::Engine => '/widget', as: :hbw

  end
end
