Class RestApi
	require 'net/http'

	def initialize
		
	end
	
	def getResponce(url)
		JSON.parse(getJsonResponce(url))	
	end

	def getJsonResponce(url)
		false unless url

		url = URI.parse(url.to_s)
    	req = Net::HTTP::Get.new(url.to_s)
    	res = Net::HTTP.start(url.host, url.port) {|http|
      		http.request(req)
    	}
    	res.body	
	end
end
